### What is this repository for? ###

COP4331C Spring 2017 : Group 2 ("The Cardigans of Nassiff")

Project: Restaurant Management Mobile APP

Team Members: Rebeca Amaya, Ashton Ansag,
Alexander Arendsen, Thomas "TJ" Cleary,
Melissa Gramajo, Vivienne Do

### How do I get set up? ###

0.How to Clone repositories by URL in VSO

    Step0.0: Obtain URL 
    "Code" (top tool bar) >> "Restaurant Management" >> "Clone" (right corner) 
    
    Step0.1: Add new repo using git command line | | SourceTree 
    
    *note: You may be prompted for username and password when adding remote repo
    username: poosusername
    password: c=299792458 
    end of note. 
    
git clone command line:

    git clone https://projectrest.visualstudio.com/_git/Restaurant%20Management

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
